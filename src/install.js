import btButton from './components/bt.vue';

export default {
  install: Vue => {
    Vue.component('bt-button', btButton);
  },
};
