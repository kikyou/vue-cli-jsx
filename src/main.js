import Vue from 'vue';
import App from './App.vue';
import tbButton from '../dist/tbButton.common';

Vue.use(tbButton);
Vue.config.productionTip = false;

new Vue({
  render: h => h(App),
}).$mount('#app');
